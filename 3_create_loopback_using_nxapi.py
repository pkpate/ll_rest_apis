"""This script connects to the NX-OS Always-On Sandbox via NXAPI-REST and configures a loopback"""

# Import required modules
import requests
import json
import os
import sys
from dotenv import load_dotenv
from urllib3 import disable_warnings, exceptions

# Suppress SSL Cert Verification Warnings
disable_warnings(exceptions.InsecureRequestWarning)

# Load Environment Variables defined in .env file
load_dotenv()
NXOS_SANDBOX = os.getenv("NXOS_SANDBOX")
USERNAME = os.getenv("NXOS_USERNAME")
PASSWORD = os.getenv("NXOS_PASSWORD")
WT_ACCESS_TOKEN = os.getenv("WEBEX_TEAMS_ACCESS_TOKEN")
WT_ROOM_ID = os.getenv("WEBEX_TEAMS_ROOM_ID")

# TODO: Please enter your loopback_id as provided by instructor
LOOPBACK_ID = 119


def get_nxos_auth_token(device_ip, device_user, device_password):
    """Login to NX-OS Device and get Authentication Token from cookies"""

    # Define URL of Service
    login_uri = '/api/mo/aaaLogin.json'
    url = f'https://{device_ip}/{login_uri}'

    # Define Headers
    headers = {'Content-Type': 'application/json'}

    # Define payload for the POST request
    payload = {
        "aaaUser": {
            "attributes": {
                "name": device_user,
                "pwd": device_password,
            }
        }
    }

    print(f'\nLogging into {device_ip}...')
    r = requests.post(url=url, headers=headers, json=payload, verify=False)
    r.raise_for_status()

    if r.ok:
        return r.cookies
    else:
        return False


def render_loopback_payload(json_file, loopback_id):
    """ Open JSON file and load as dictionary.  Update various keys of payload with specific loopback_id based parameters"""
    with open(json_file) as f:
        payload = json.load(f)

    payload['topSystem']['children'][0]['ipv4Entity']['children'][0]['ipv4Inst']['children'][0]['ipv4Dom']['children'][0]['ipv4If']['attributes']['id'] = f'lo{loopback_id}'
    payload['topSystem']['children'][0]['ipv4Entity']['children'][0]['ipv4Inst']['children'][0]['ipv4Dom']['children'][0]['ipv4If']['children'][0]['ipv4Addr']['attributes']['addr'] = f'192.168.100.{loopback_id}/32'
    payload['topSystem']['children'][1]['interfaceEntity']['children'][0]['l3LbRtdIf']['attributes']['descr'] = 'll_rest_apis Created by NXAPI-REST'
    payload['topSystem']['children'][1]['interfaceEntity']['children'][0]['l3LbRtdIf']['attributes']['id'] = f'lo{loopback_id}'

    return payload


def create_loopback(device_ip, loopback_id, auth_cookie):
    """Create loopback interface on NXOS Device"""

    # Define URL of Service
    sys_uri = '/api/mo/sys.json'
    url = f'https://{device_ip}/{sys_uri}'

    # Define Headers
    headers = {'Content-Type': 'application/json'}

    # Define payload for the POST request
    # TODO:  The payload for this request is pretty large.  So we've defined it in file named 'loopback_payload.json.'
    #        We have also defined a function named render_loopback_payload() that opens the file and updates
    #        various keys with values based on the Loopback ID assigned to you.
    #        Please call that function below and pass in two arguments, the name of the json file and your the
    #        locally scoped loopback_id
    payload = render_loopback_payload('loopback_payload.json', loopback_id)

    # Connect to device
    print(f'\nCreating loopback{loopback_id} on {device_ip}...')
    r = requests.post(url=url, headers=headers, json=payload, verify=False, cookies=auth_cookie)
    r.raise_for_status()

    if r.ok:
        return True
    else:
        return False


def post_message(message):
    """Post message in Webex Teams Room upon successful completion of exercise"""

    # Define URL of Service
    messages_uri = '/v1/messages'
    url = f'https://webexapis.com/{messages_uri}'

    # Define Headers
    headers = {'Content-Type': 'application/json',
               'Authorization': f'Bearer {WT_ACCESS_TOKEN}'}

    # Define payload for the POST request
    payload = {'roomId': WT_ROOM_ID,
               'text': message}

    print('\nPosting message to Webex Teams Room...')
    r = requests.post(url=url, headers=headers, json=payload)
    r.raise_for_status()

    if r.ok:
        print('\nThe following message was posted to the Webex Teams Room:')
        print(message)


if __name__ == '__main__':
    cookie = get_nxos_auth_token(device_ip=NXOS_SANDBOX,
                                 device_user=USERNAME,
                                 device_password=PASSWORD)
    if not isinstance(LOOPBACK_ID, int):
        sys.exit('Please ensure variable LOOPBACK_ID is defined as an integer')
    created = create_loopback(device_ip=NXOS_SANDBOX, loopback_id=LOOPBACK_ID, auth_cookie=cookie)
    if created:
        print('Success!')
        msg = f'''I successfully created loopback{LOOPBACK_ID} on {NXOS_SANDBOX}!'''
        post_message(message=msg)
    else:
        print('Something went wrong when attempting to create a loopback.')
        print('This may be due to unreliability of the environment.  Try again.')
