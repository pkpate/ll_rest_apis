"""This script gets a random dad joke and posts it in a Webex Teams Room."""

# Import required modules
import requests
import os
from dotenv import load_dotenv

# Load Environment Variables defined in .env file
load_dotenv()
WT_ACCESS_TOKEN = os.getenv("WEBEX_TEAMS_ACCESS_TOKEN")
WT_ROOM_ID = os.getenv("WEBEX_TEAMS_ROOM_ID")


def get_dad_joke():
    """Get random dad joke from icanhazdadjoke.com"""

    # Define URL of Service
    url = 'https://icanhazdadjoke.com'

    # Define Headers
    headers = {'Accept': 'application/json'}

    # Make a GET request to URL
    print('\nGetting random dad joke...')
    r = requests.get(url=url, headers=headers)

    # TODO 1: Parse the response to extract the string containing the random dad joke
    # Hint #1:  Try printing the response by using print(r.json()) or print(r.text)
    # Hint #2:  r.json() returns a python dictionary!

    dad_joke = r.json()['joke']

    return dad_joke


def post_message(message):
    """Post message in Webex Teams Room upon successful completion of exercise"""

    # Define URL of Service
    messages_uri = '/v1/messages'
    url = f'https://webexapis.com/{messages_uri}'

    # Define Headers
    headers = {'Content-Type': 'application/json',
               'Authorization': f'Bearer {WT_ACCESS_TOKEN}'}

    # Define payload for the POST request
    payload = {'roomId': WT_ROOM_ID,
               'text': message}

    print('\nPosting message to Webex Teams Room...')
    r = requests.post(url=url, headers=headers, json=payload)
    r.raise_for_status()

    if r.ok:
        print('\nCongratulations! The following message was posted to the Webex Teams Room:')
        print(message)


if __name__ == '__main__':
    joke = get_dad_joke()

    if not isinstance(joke, str):
        print(f'The data returned from the get_dad_joke() function is {type(joke)}.')
        print(f'Please parse the dad joke response correctly...')

    # TODO 2:  Call the post_message function to post the dad joke into the Webex Teams Room
    post_message(joke)
